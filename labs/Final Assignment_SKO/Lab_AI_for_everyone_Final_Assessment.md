<img src="images/IDSNlogo__1_.png" width="200" height="200"/>

## Exercise: Sales Promotion using a Data Modeler

#### Step 1: Create an IBM Cloud Account 

1. Go to: [Create a free account on IBM Cloud ](https://cloud.ibm.com/registration)

2.  In the **Email** box, enter your email address and then click the arrow.

<img src="images/Picture2.png">


3. When your email address is accepted, enter your **First Name**, **Last Name**, **Country** or Region**, and create a **Password**.

    Note: To get enhanced benefits, please sign up with your company email address rather than a free email ID like Gmail,      Hotmail, etc.

    If you would like IBM to contact you for any changes to services or new offerings, then check the box to accept the option to be notified by email.

4. Click **Create Account** to create your IBM Cloud account.

**Task 2: Confirm your email address**

1.  An email is sent to the address that you signed up with.

<img src="images/Picture3.png">

2.  Check your email, and in the email that was sent to you, click **Confirm Account**.

<img src="images/Picture4.png">

3.  You will receive notification that your account is confirmed.

<img src="images/Picture5.png">

Click **Log In**, and you will be directed to the IBM Cloud Login Page.

**Task 3: Login to your IBM Cloud account**

1. On the Log in to [IBM Cloud page](https://cloud.ibm.com/login), in the **ID** box, enter your email address and then click **Continue**. 

<img src="images/Picture6.png">

2.  In the **Password** box, enter your password, and then click **Log in**.

<img src="images/Picture7.png">

#### Step 2: Create a Watson Studio Resource

**Scenario**

To manage all your projects, you will use IBM Watson Studio. In this exercise, you will add Watson Studio as a Resource.

**Task 1: Add Watson Studio as a resource**

1.  Click on this link to go to the Watson Studio service in the IBM Cloud catalog. 

Alternatively . . . On the Dashboard, click **Create Resource**.

<img src="images/Picture8.png">

2.  In the Catalog, click **AI (16)**.

<img src="images/Picture9.png">

Note that the **Lite** Pricing plan is selected.

3.  In the list of **Services**, click **Watson Studio**.

<img src="images/Picture10.png">

4.  On the Watson Studio page, select the region closest to you, verify that the **Lite** plan is selected, and then click **Create**.

<img src="images/Picture11.png">

5.  When the Watson Studio resource is successfully created, you will see the Watson Studio page. Click **Get Started**.

<img src="images/Picture13.png">

6.  You will see this message when Watson Studio is successfully set up for you.

<img src="images/Picture14.png">

#### Step 3: Create a Project

To create a Watson Machine Learning instance, click on **Create a Project** and then click on **Create an Empty Project**.

<img src="images/Picture15.png">

<img src="images/Picture16.png">

Now let's fill in some project details and click **Create**. The IBM Cloud Object Storage, which provides you storage for your images, should be automatically created for you.

After creating your project, by default, you will land on the Projects page of your project. Click Add to Project. 

When the Choose Asset Type box pops up select Modeler flow in the list.

<img src="images/Picture17.png">

After creating your project, by default, you will land on the **Projects** page of your project. Click **Add to Project**. 

When the **Choose Asset Type** box pops up select **Modeler flow** in the list.

<img src="images/Picture18.png">

On the next screen, choose **From Example**, then choose **Sales Promotion Study** from the options.

<img src="images/Picture19.png">

#### Step 4: Explore Sales Promotion Study Model 

After creating your project, by default, you will land on the page where you can view and explore the sales promotion study model.

Right-click the purple box labeled Goods1n and then choose **Preview**.

<img src="images/Picture20.png">

At this point, you’ve just set up the model flow. It’s a visual representation of the path the data will take and what decisions will be made at each node in order to determine which promotions will lead to an increase in revenue. That is, this is the model that we’ll feed data to in order to get a predictive result. In the next step, you’ll feed the model some sales data and get the result for each one.

The preview button shows the data set before you run it through the model. Note that this is just a sample of the data and the preview shows just a few rows of the data. The actual data set is much larger. This data set will be used to train the model. Notice there is no prediction about how much of an increase the promotion will make on revenue. We’ll get the result once we run the model.

<img src="images/Picture21.png">

#### Step 5: Run Sales Promotion Study Model 

Now that you’ve explored the model and have begun to understand the underlying data and algorithm, we will run the model to view and analyze the outputs. When you run the models, you will get two outputs: one using a neural network to predict the increase and the other using a classification and regression tree approach. The main difference between regression and a neural network is the impact of change on a single weight. In regression, you can change a weight without affecting the other inputs in a function. However, this isn’t the case with neural networks. Since the output of one layer is passed into the next layer of the network, a single change can have a cascading effect on the other neurons in the network.

Towards the top of the screen, press the **Play** button, the models should render.

#### Step 6: Check Out the Results!

Once you have run the model, machine learning will predict the increase in revenue for each product after each promotion has been applied. Besides the promotion it recommends for each product, it will also give you a percentage increase for each model. You can examine both models to see which predicts the increase with greater accuracy.

**Task 1: Look at the Neural Network model**

Right click the top brown box called **Increase**, and click **Preview**.

<img src="images/Picture22.png">

Notice the fields labeled **Increase** and **$N-Increase**. The Increase column shows the actual percentage increase between the values from before and after the promotion was run (the **Before** and **After** columns). The **$N-Increase** column was created by the modeler using the data in the **Cost**, **Promotion**, and **Before** columns to predict the percentage increase for each product based on the promotion. Because the test data included the revenue before and after the promotion, Watson learns from that data and is able to predict the impact of the promotion when the **After** data column is not available. The **$N-Increase** field is the prediction of the percentage increase based on the Neural Net model. **Increase** shows the actual increase in revenue and **$N-Increase** shows the prediction from running the neural network model. You can compare the actual increase with the predicted increase to see the accuracy of the neural net prediction. You can see in the screenshot that the prediction is very close for each product.

<img src="images/Picture21__1_.png">


**Task 2: Save a screenshot**

NOTE: This step is optional for those auditing the course. The screenshot saved in this step will be required as part of the Graded Final Assignment for those pursuing a certificate for this course.

Take and save screenshots in .jpeg or .jpg format of the output of the model. Ensure the labels and confidence scores (the $N-Increase column) in the table are readable. See a sample screenshot above.

**Task 3: Look at the Regression Tree model**

Next, right click the bottom brown box called **Increase**, and click **Preview**.

<img src="images/Picture24.png">

Notice the field labeled **$R-Increase**. As with the Neural Net model, the bamount was created by the C&RT (Classification and Regression Tree) model and shows the predicted percentage increase in revenue based on running the promotion for each product.

<img src="images/Picture25.png">

As with the neural net model, you can compare the **Increase** column and the **$R-Increase** column to see how accurately the regression model predicted the revenue increase.

You should also right click each model and choose **View Model** to explore information about each of the models and their outputs. By examining the models, you can determine which is likely to give you greater accuracy in the prediction of percentage increases by running the promotions.

<img src="images/Picture26.png">

**Task 4: Save a screenshot**

NOTE: This step is optional for those auditing the course. The screenshot saved in this step will be required as part of the Graded Final Assignment for those pursuing a certificate for this course.

Take and save screenshots in .jpeg or .jpg format of the output of the model. Ensure the labels and confidence scores (the $R-Increase column) in the table are readable. See a sample screenshot above.

#### Step 7: Test Your Understanding

Answer the question: Which model has better accuracy each product and why?

**Hint:** Consider how neural networks and regression models process data and why they might produce different results.

#### Step 8: Share Your Results!

Follow us on Twitter and send us some of your funniest and most interesting results you found with IBM Data Modeler Flow!

<img src="images/Picture27.png">


## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2020-09-07 | 2.0 | Arpita | Migrated Lab to Markdown and added to course repo in GitLab |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>






